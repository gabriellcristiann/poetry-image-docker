# [Python Poetry](https://github.com/python-poetry/poetry) in Docker

Imagens Docker `python-poetry` robustas, leves e configuráveis para qualquer uso
caso.

## Guia de início rápido

Você pode usar essa imagem como uma imagem base para o seu próprio Dockefile.

```Dockerfile
FROM registry.gitlab.com/gabriellcristiann/poetry-image-docker

RUN poetry --version
# Your build steps here.
```

Porém você também pode baixar e executar a imagem mais recente do Docker puxando do Container Registry.
```
docker pull registry.gitlab.com/gabriellcristiann/poetry-image-docker
docker run --rm registry.gitlab.com/gabriellcristiann/poetry-image-docker poetry --version
```

## License

This project is licensed under the terms of the [MIT License](https://choosealicense.com/licenses/mit/).